<?php 

/**
Tabla de hash para guardar numeros y luego saber su posicion exacta en un vector
Usando la funcion (2*n+3) mod 10
donde n es el numero a guardar
genere la funcion hash que retorne la clave y le indique en que posicion del vector donde guardara los siguientes valores.

{9, 13, 5, 39, 21, 18, 28, 43, 30, 10}

ejemplo con el primero valor (2*9+3) mod 10

solucion (21) mod10 = 1

el valor 9 se guardara en la posicion 1 del vector.
 */

/**
Pasos:

1 crear la funcion hash y probar que retorne el mismo valor cuantas veces la llamemos.
2 crear el array con los datos entregados.
3 crear la tabla de hash y recorrer el array creado en el paso 2 para asignarlo a la tabla hash.
4 usar la funcion para buscar un numero en la tabla hash.

 */

 function wilsonkey($numero){
    $resultado = 0;
    $resultado = 2*$numero+3;
    $resultado = $resultado%10;
    return $resultado;
 }

//  echo wilsonkey(9);
//  echo '</br>';
//  echo wilsonkey(28);
//  echo '</br>';
//  echo wilsonkey(28);

$datosEntregados = array(9, 13, 5, 39, 21, 18, 28, 43, 30, 10);

$tablaHash = array();

for ($i=0; $i < count($datosEntregados); $i++) { 
    $keyhash = wilsonkey($datosEntregados[$i]);
    $tablaHash[$keyhash][] = $datosEntregados[$i];
}

var_dump($tablaHash);

$busqueda = wilsonkey(30);

echo "Cuantos hay en tablaHash[busqueda] " . count($tablaHash[$busqueda]) . "</br>";

for ($i=0; $i < count($tablaHash[$busqueda]); $i++){
    if($tablaHash[$busqueda][$i] == 30){
        echo "Encontre el 30 en la posicion hash " . $busqueda . " de " . ($i+1);
    }
}

?>
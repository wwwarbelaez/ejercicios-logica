<?php

// inicializar un array
$valoresNumericos = array();

// ingresar valores al array

$valoresNumericos[0] = 200;
$valoresNumericos[1] = 300;
$valoresNumericos[2] = 500;

var_dump($valoresNumericos);

echo $valoresNumericos[1] . "<br>";

$valoresNumericos[1] = 350;

echo $valoresNumericos[1] . "<br>";

$arreglo = array();
for ($i=0; $i <= 9; $i++) { 
    $arreglo[$i] = "Valor " . ($i + 1);
}

var_dump($arreglo);

for ($j=0; $j <= 10; $j++) { 
    echo $arreglo[$j] . "<br>";
}


// $arregloNuevo = array();
// for ($i=0; $i < 10; $i++) {
//     $valor = readline("Ingrese un valor para guardar en memoria: ");
//     $arregloNuevo[$i] = $valor;
// }


// crear 4 array con 3 posiciones, cada uno con un tipo dato diferente (int, float, string, boolean)
// pintar los valores de los 4 array con un solo for

?>
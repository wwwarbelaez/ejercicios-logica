## Repositorio temas para practicar y enseñar

**Variables**
>    Que es (una variable es un espacio en memoria que se puede reservar o usar directamente) <br/>
>    Tipos (int, double o float, string, arrays, boolean) (en esta parte se ven todas menos los arrays porque tienen un segmento)<br/>
>    Declaracion y asignacion [declaracion var miVar; asignacion var miVar = "Hola";]<br/>

**primer-ejercicio**
> data = prompt("MSG", "DEFAULT");<br/>
> var entero = parseInt(string, base);<br/>
> var doubleFloat = parseFloat(cadena);<br/>

**Operadores**
>    Aritmeticos (+ - * / % ^)<br/>
>    Jerarquia de operadores 1[()] 2[^] 3[* /] 4 [+ -]<br/>
>    Agrupacion de operadores <br/>

**Condicionales**
>    simples if<br/>
>    condiones dobles if else<br/>
>    multiples if elseif<br/>
>    condiciones anidadas if if <br/>
>    switch <br/>
    
**Ciclos**
>    for - ejercicio tabla de multiplicar<br/>
>    while - ejercicio adivina el numero - menu <br/>

**Estructuras de datos**
>    Arrays<br/>
>    Arrays multidimencionales<br/>
>    Ejercicio con arrays<br/>
>    Otras estructuras de datos<br/>
    
**Funciones**
>    Funciones<br/>
>    Estructura<br/>
>    Ejercicio: encontrar el numero mayor<br/>
>    Ejercicio cajero automatico<br/>

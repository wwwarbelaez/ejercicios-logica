<?php
    // El problema consiste en escribir en letras el nombre del mes
    // de acuerdo al número que le corresponde: 
    // 1 (Enero), 2 (Febrero), hasta el mes 12 (Diciembre)

    /**
     * receta
     * 
     * 1. variable que me indica un numero del 1 al 12 que corresponde a cada mes.
     * 2. variable que me guarde el resultado.
     */
    $numeroMes = 2;
    $resultadoMes = "";

    switch ($numeroMes) {
        case 1:
            $resultadoMes = "Enero";
            break;
        case 2:
            $resultadoMes = "Febrero";
            break;
        case 3:
            $resultadoMes = "Marzo";
            break;
        case 4:
            $resultadoMes = "Abril";
            break;
        case 5:
            $resultadoMes = "Mayo";
            break;
        case 6:
            $resultadoMes = "Junio";
            break;
        case 7:
            $resultadoMes = "Julio";
            break;
        case 8:
            $resultadoMes = "Agosto";
            break;
        case 9:
            $resultadoMes = "Septiembre";
            break;
        case 10:
            $resultadoMes = "Octubre";
            break;
        case 11:
            $resultadoMes = "Noviembre";
            break;
        case 12:
            $resultadoMes = "Diciembre";
            break;
        default:
            $resultadoMes = "Ingrese un numero del 1 al 12";
            break;
    }

    echo $resultadoMes;

    echo 2/0;


?>
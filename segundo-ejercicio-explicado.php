<?php
/**
    *Diseñe un algoritmo que determine si ún número es o no es, par positivo.
    *Se usa modulo y condicionales if else

    1. saber si el numero es positivo
    2. determinar si es o no es par
    modulo = %
 */

    $numero = 11;
    $modulo = 0;
    if($numero > 0){
        // es o no es par
        $modulo = $numero % 2;
        echo "modulo " . $modulo;
        if($numero % 2 == 0){
            echo "<br/>El numero es par";
        }else{
            echo "<br/>El numero es impar";
        }
    }else{
        echo "El numero NO es positivo";
    }


    // if(condicion){
    //     #codigo
    // }else if(condicion){
    //     #codigo
    // }else if(condicion){
    //     #codigo
    // }else{
    //     #codigo
    // }

    $diasRecorridos = 35;
    $diasSinPago = "";

    if($diasRecorridos < 30){
        $diasSinPago .= "30 ";
    }else if($diasRecorridos > 30 && $diasRecorridos <= 60){
        $diasSinPago .= "de 30 a 60";
    }else if($diasRecorridos > 60 && $diasRecorridos <= 90){
        $diasSinPago .= "de 60 a 90";
    }else{
        $diasSinPago .= " mayor a 90";
    }

    $nombre = "wilson";
    $valor = "";
    if(strpos("w", $nombre) !== false){
        $valor .= "Tiene la w ";
    }
    if(strpos("n", $nombre) !== false){
        $valor .= "Tiene la n ";
    }

    echo "<br/><br/><br/>" . $diasSinPago;


?>
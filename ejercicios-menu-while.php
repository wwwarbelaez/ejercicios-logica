<?php
    /**
     * Crear un menu de opciones que permita seleccionar un opcion por medio de su numero.
     * Como se muestra a continuacion.
     * NOTA: Buscar como limpiar la pantalla
     * 
     * 1. Numero de dijitos
     * 2. Numeros con limite
     * 3. Suma de valores
     * 4. Sueldos empleados
     * 5. Salir
     */

     /**
      * 1. Escribir un programa que solicite la carga de un número entre 0 y 999, y nos muestre un mensaje de cuántos
      * dígitos tiene el mismo. Finalizar el programa cuando se cargue el valor 0.
      *
      * 2. Escribir un programa que solicite la carga de un valor positivo y nos muestre desde 1 hasta el valor 
      *    ingresado de uno en uno. Ejemplo: Si ingresamos 30 se debe mostrar en pantalla los números del 1 al 30.

      * 3. Desarrollar un programa que permita la carga de 10 valores por teclado y nos muestre posteriormente 
      *    la suma de los valores ingresados y su promedio.

      * 4. En una empresa trabajan n empleados cuyos sueldos oscilan entre $100 y $500, 
      *    realizar un programa que lea los sueldos que cobra cada empleado e informe cuántos
      *    empleados cobran entre $100 y $300 y cuántos cobran más de $300. Además el programa
      *      deberá informar el importe que gasta la empresa en sueldos al personal.
      */


?>
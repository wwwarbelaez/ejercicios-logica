<?php
// Tablas de hash

/**
Una tabla hash, 
matriz asociativa,
hashing, 
mapa hash, 
tabla de dispersión o tabla fragmentada 

es una estructura de datos que asocia llaves o claves con valores. 
 */

// [clave] = [array de valores]; 

// $variable = array();

// $variable['wilson'] = "1001579793";

// $variable['wilson'] = [
//     'mama' => "Nombre",
//     'papa' => "",
//     'edad' => "21"
// ]

// shash('wilson');

// function shash($clave) {
//     return $variable[$clave];
// }

// echo "Wilson <br>";
// echo sha1('wilson');


// $misDatos = array(0 => "Hola", 1 => "Como", 2 => "Estas");

// function newkey($valor){
//     $nombre = 'wilson';
//     $letra = 'w';
//     return $letra;
// }

// $key = newkey('wilson');

// $Datos[$key] = $valor;

// $key = newkey('wildeman');
// $Datos[$key] = $valor;


// // w = ['wilson', 'wildeman']

// $key = newkey('wilson');

// $nombrePorW = $Datos[$key];



// ejercicio

function hashkey($nombre) {
    $primeraLetra = '';
    $primeraLetra = substr($nombre, 0, 1);
    return $primeraLetra;
}

$Data = array();

$key = hashkey('wilson');
$Data[$key][0] = array('nombre' => 'Wilson', 'edad' => '21', 'estado civil' => 'soltero');

$key = hashkey('wildeman');
$Data[$key][1] = array('nombre' => 'Wildeman', 'edad' => '21', 'estado civil' => 'soltero');

$key = hashkey('adriana');
$Data[$key][0] = array('nombre' => 'Adriana', 'edad' => '20', 'estado civil' => 'casada');

$key = hashkey('jorge');
$Data[$key][0] = array('nombre' => 'Jorge', 'edad' => '19', 'estado civil' => 'casado');

$key = hashkey('amparo');
$Data[$key][1] = array('nombre' => 'Amparo', 'edad' => '18', 'estado civil' => 'soltera');


// $key = hashkey('wilson');


// var_dump($Data[$key]);

var_dump($Data);

?>
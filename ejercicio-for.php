<?php
    // Generar la tabla del 2 usando el ciclo for
    $j = 0;
    for($i=0; $i <= 20; $i = $i + 2){
        echo "2 * " . $j . " = " . $i;
        echo "<br>";
        $j = $j + 1;
    }

    /**
     * Generar las tablas de multiplicar del 1 al 10
     * 
        1 * 0 = 0
        1 * 1 = 1
        1 * 2 = 2
        1 * 3 = 3
        1 * 4 = 4
        1 * 5 = 5
        1 * 6 = 6
        1 * 7 = 7
        1 * 8 = 8
        1 * 9 = 9
        1 * 10 = 10
---------------------------
        2 * 1 = 2
        2 * 2 = 4
        2 * 3 = 6
        2 * 4 = 8
        2 * 5 = 10
        2 * 6 = 12
        2 * 7 = 14
        2 * 8 = 16
        2 * 9 = 18
        2 * 10 = 20
     * 
     */

?>
<?php
// Funcion trozo de codigo que generalmente se repite y se quiere escribir una sola vez.

// echo "Hola </br>";

// echo "Hola </br>";

function saludar($nombre = "el nombre"){

    echo "Hola ". $nombre ."</br>";
}

saludar("Wilson");
saludar();

function sumar($a, $b){
    return $a + $b;
}

$misuma = sumar(5, 5);

echo $misuma . "</br>";

$misuma = sumar(3, 3);

echo $misuma . "</br>";



?>